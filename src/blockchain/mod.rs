use std::convert::{From, Into};
use std::vec::{Vec};
use std::collections::HashMap;
use std::time::{SystemTime};
use blake2::{Blake2b, Digest};
use log::{info, warn};
use std::fmt::Display;
use std::io::Bytes;
use std::borrow::BorrowMut;
use openssl::sign::{Signer, Verifier};
use openssl::rsa::Rsa;
use openssl::pkey::PKey;
use openssl::hash::MessageDigest;
use base64;


/// Base building item of the Blockchain
#[derive(Clone, Debug)]
pub struct Block {
    pub nonce: u128,
    pub transactions: Vec<Transaction>,

    hash: Option<String>,
    prev_hash: Option<String>,
}

impl Block {
    pub fn new () -> Self {
        return Self {
            transactions: Vec::new(),
            nonce: 0,
            hash: None,
            prev_hash: None,
        }
    }

    /// Will check if the hash is set making the block final
    pub fn is_finalized(&self) -> bool {
        !self.hash.is_none()
    }

    // Checks if the block is finalized AND has a previous hash set
    pub fn is_part_of_chain(&self) -> bool {
        self.is_finalized() && self.prev_hash.is_some()
    }

    pub fn get_transaction_count(&self) -> usize {
        self.transactions.len()
    }

    /// Will hash all transactions and add result to the hash member
    /// Returns an error or the hash code on success
    /// `prev_hash` needs to be set
    fn finalize(&mut self) {
        if self.prev_hash.is_some() {
            let hash = self.calculate_hash_str();
            self.hash = Some(hash.clone());
        }
    }

    /// Wrapper for :calculate_hash but turns result into string
    pub fn calculate_hash_str(&self) -> String {
        let hash = self.calculate_hash();
        let result = hash.iter().map(|&c| c as char).collect();

        return result;
    }

    /// Will generate a hash code by taking all transactions and nonce into account
    pub fn calculate_hash(&self) -> Vec<u8> {
        let mut hasher = Blake2b::new();
        let transaction_hash = self.calculate_transaction_hash();
        hasher.update(transaction_hash);
        hasher.update(self.nonce.to_be_bytes());
        return Vec::from(hasher.finalize().as_ref());
    }

    pub fn calculate_transaction_hash(&self) -> Vec<u8>{
        let mut hasher = Blake2b::new();
        for (i, transaction) in self.transactions.iter().enumerate() {
            hasher.update(transaction.calculate_hash().as_bytes());
        }

        return Vec::from(hasher.finalize().as_ref());
    }
}

/// A single entry that represents some performable action
#[derive(Clone, Debug, PartialEq)]
pub struct Transaction {
    nonce: u128,
    from: String, // account id
    time: SystemTime,
    signature: Option<String>,
    record: BlockAction
}

impl Transaction {
    pub fn new(from: String, record: BlockAction, nonce: u128) -> Self {
        return Self {
            nonce,
            from,
            record,
            time: SystemTime::now(),
            signature: None
        }
    }

    pub fn calculate_hash(&self) -> String {
        let mut hasher = Blake2b::new();
        hasher.update(self.from.as_bytes());
        hasher.update(self.nonce.to_be_bytes());

        /*
        if self.signature.is_some() {
            let val = self.signature.as_ref().unwrap();
            hasher.update(val.as_bytes());
        } else {
            hasher.update("unsigned transaction".as_bytes());
        }
        */

        let mut time_str: String = "".into();
        let time_duration = self.time.duration_since(
            SystemTime::UNIX_EPOCH);
        if let Ok(time) = time_duration {
            time_str = time.as_millis().to_string();
        } else {
            time_str = "Time conversion error".into();
        }

        //println!("{}", time_str);

        hasher.update(time_str);

        match &self.record {
            BlockAction::CreateUserAccount(acc_id) => {
                hasher.update("CreateUserAccount".as_bytes());
                hasher.update(acc_id.as_bytes());
            },
            BlockAction::ChangeStoreValue {key, value} => {
                hasher.update("ChangeStore".as_bytes());
                hasher.update(key.as_bytes());
                hasher.update(value.as_bytes());
            },
            BlockAction::RemoveAccount(acc) => {
                hasher.update("RemoveAccount".as_bytes());
                hasher.update(acc.as_bytes());
            },
            BlockAction::TransferTokens {to, amount} => {
                hasher.update("TransferTokens".as_bytes());
                hasher.update(to.as_bytes());
                hasher.update(amount.to_string().as_bytes());
            },
            BlockAction::CreateTokens {receiver, amount} => {
                hasher.update("CreateTokens".as_bytes());
                hasher.update(receiver.as_bytes());
                hasher.update(amount.to_string().as_bytes());
            },
        }
        return hasher.finalize().iter().map(|&c| c as char).collect();
    }

    /// Will sign the transaction using the DER private key
    /// (Stored in `signature`
    pub fn sign_transaction(&mut self, private_key_der: &[u8])  -> bool {
        let hash = self.calculate_hash().clone();
        let hash = hash.as_bytes();
        let pkey = PKey::private_key_from_der(private_key_der).unwrap();
        let mut signer = Signer::new(MessageDigest::sha256(), &pkey).unwrap();
        signer.update(hash);
        let signature = base64::encode_config(signer.sign_to_vec().unwrap(), base64::URL_SAFE_NO_PAD);
        // signer.sign_to_vec().unwrap().iter().map(|&c| c as char).collect();
        self.signature = Some(signature);

        true

    }

    /// Will check if the transaction is signed by the given key
    pub fn check_signature(&self) -> bool {
        if self.is_signed() {
            let pub_key_der_base64 = self.from.clone();
            let pub_key_der = base64::decode_config(pub_key_der_base64.clone(), base64::URL_SAFE_NO_PAD).unwrap();
            let hash = self.calculate_hash().clone();
            let hash = hash.as_bytes();
            let pkey = PKey::public_key_from_der(pub_key_der.as_ref()).unwrap();
            let mut verifier = Verifier::new(MessageDigest::sha256(), &pkey).unwrap();
            let signature = base64::decode_config(self.signature.as_ref().unwrap(), base64::URL_SAFE_NO_PAD).unwrap();
            verifier.update(hash).unwrap();
            return verifier.verify(&signature).unwrap();
        }
        false
    }

    // Checks if a signature is set
    pub fn is_signed(&self) -> bool {
        self.signature.is_some()
    }
}

/// This stores our ledger which is a chain of block instances
#[derive(Debug)]
pub struct BlockChain {
    blocks: Vec<Block>,
    accounts: HashMap<String, Account>,
    max_transactions_per_block: usize,
    pending_transactions: HashMap <String, Transaction>
}

impl BlockChain {
    // Will create a blockchain with one block containing the given user ids
    pub fn new(mut genesis: Block, initial_accounts: &Vec<Account>) -> Self {
        let mut i: u32 = 0;
        let mut transactions: Vec<Transaction> = Vec::new();

        for acc in initial_accounts {
            let create_user_transaction_record = BlockAction::CreateUserAccount(acc.key.clone());
            let create_tokens_transaction_record = BlockAction::CreateTokens {
                receiver: acc.key.clone(),
                amount: acc.balance};

            i += 1;
            let create_user_transaction = Transaction::new("0".into(),
                                                                       create_user_transaction_record,
                                                                       i.into());
            i += 1;
            let create_tokens_transaction = Transaction::new("0".into(), create_tokens_transaction_record, i.into());
            transactions.push(create_user_transaction);
            transactions.push(create_tokens_transaction);
        }

        genesis.transactions.append(&mut transactions);
        //genesis.prev_hash = Some("0".into());
        //genesis.finalize();

        let mut bc = Self {
            accounts: HashMap::new(),
            blocks: Vec::new(),
            max_transactions_per_block: 10,
            pending_transactions: HashMap::new()
        };

        bc.add_block(genesis);
        bc
    }

    pub fn get_user_data(&self, id: String) -> Option<&Account> {
        if let Some(acc) = self.accounts.get(&id) {
            return Some(acc);
        }
        None
    }

    pub fn add_transaction_request(&mut self, transaction: Transaction) -> Result<String, String> {
        let t_hash = transaction.calculate_hash();
        if self.pending_transactions.contains_key(t_hash.as_str()) {
            return Err("Transaction is already inside the queue! (Code: 289734589)".into());
        }

        if !self.could_execute_transaction(&transaction) {
            return Err("This transaction is not valid for execution (Code: 845738945)".into());
        }

        self.pending_transactions.insert(t_hash, transaction);

        Ok("Added".into())
    }

    /// Returns a block with transactions from the pending transactions
    /// Will return up to maximum transaction limit -1 block
    /// you may or may not add a transaction which may
    /// yield 1 token for any account if you wish
    pub fn generate_block_with_transactions(&self) -> Result<Block, String> {
        let mut empty_block = Block::new();
        for (i, (_id, transaction)) in self.pending_transactions.iter().enumerate() {
            if i < self.max_transactions_per_block - 1 {
                empty_block.transactions.push(transaction.clone());
            }
        }

        empty_block.prev_hash = self.blocks[self.n_blocks()-1].hash.clone();

        Ok(empty_block)
    }

    /// Checks all block hashes and pointers to previous hashes
    pub fn is_valid(&self) -> bool {
        let mut prev_hash: String = "0".into();

        for (i, block) in self.blocks.iter().enumerate() {
            let block_hash = block.hash.as_ref().unwrap().clone();
            let rehashed = block.calculate_hash_str();
            if block_hash != rehashed {
                return false;
            }

            if *(block.prev_hash.as_ref().unwrap()) != prev_hash {
                return false;
            }

            // Check transaction signed
            for transactions in block.transactions.iter() {
                if transactions.is_signed() {
                    if !transactions.check_signature() {
                        return false
                    }
                }
            }
            prev_hash = block_hash;
        }

        true
    }

    pub fn add_block(&mut self, mut block: Block) -> Result<String, String> {
        if self.n_blocks() == 0 {
            // We will always accept the genesis block no matter what
            block.prev_hash = Some("0".into());
            block.finalize();
        } else {
            // Check amount of transactions
            if block.get_transaction_count() > self.max_transactions_per_block {
                return Err(
                    "Cannot not accept block due to too many transactions \
                    (Code: 832409238490)".into());
            }

            // Check if given prev block hash matches actual prev block hash
            if block.prev_hash.is_some() && block.prev_hash.clone().unwrap() != self.blocks[self.n_blocks()-1].calculate_hash_str() {
                return Err("Block could not be added since previous hash does not match! (Code: 340982390)".into());
            }

            // hash it again and check if PoW is satisfied
            let hash = block.calculate_hash_str().clone();
            if !(hash.as_bytes()[0] == 82  && hash.as_bytes()[1] == 73 /*&& hash.as_bytes()[2] == 67*/){
                return Err(format!("The Block hash is {}, but I need it to start with RI (Ascii) (Code: 23423874923)", hash));
            }

            // Check each transaction for (i) being pending and (ii) being performable
            let mut t_num = 1;
            for transaction in &block.transactions {
                // Check if transaction is in pool
                if !self.pending_transactions.contains_key(transaction.calculate_hash().as_str()) {
                    return Err(format!("Transaction with id {} ({:?}) is not known to the system, \
                                 hence I will not accept that block (Code: 8378932748932)",
                                transaction.calculate_hash(), transaction));
                }

                if !self.could_execute_transaction(transaction) {
                    return Err(format!("Transaction #{} is not executable (Code: 2342342345)",
                                       t_num));

                    // @TODO at some point remove that guy from pending transactions
                    // or we may see it forever again
                }
                t_num += 1;
            }

            //let prev_block = self.blocks.last().unwrap();
            //block.prev_hash = prev_block.hash.clone();
        }

        let failed_transactions = self.execute_block(&mut block);
        for failed_transaction in failed_transactions {

        }

        block.hash = Some(block.calculate_hash_str());
        self.blocks.push(block);
        Ok("Added Block Block".into())
    }

    /// Will execute each transaction from the block in order
    /// Returns copies to the transactions which are NOT included
    ///
    /// ** Hint
    /// If a transaction is not executable it will just ignore them
    pub fn execute_block(&mut self, block: &mut Block) -> Vec<Transaction> {
        let mut t_num = 1;
        let mut not_working_transactions: Vec<Transaction> = Vec::new();
        let mut not_working_transaction_indices: Vec<usize> = Vec::new();
        {
            for transaction in &block.transactions {
                if !self.could_execute_transaction(&transaction) {
                    //panic!("Could not execute Transaction #{} ({:?})(Code: 23842903)", t_num, transaction)
                    not_working_transactions.push(transaction.clone());
                    not_working_transaction_indices.push(t_num - 1);

                    warn!(target: "Blockchain",
                          "Could not execute transaction #{} ({:?}). \
                          I will remove it from the chain and recalculate Block Hash then \
                          (Code: 28347823)", t_num, transaction)
                } else { // @TODO Should rather make a change set which can be rolled back on intermediate error
                    match &transaction.record {
                        BlockAction::CreateUserAccount(id) => {
                            let acc = Account::new(
                                AccountType::User,
                                id.clone(),
                                0);

                            self.accounts.insert(id.clone(), acc);
                        },
                        BlockAction::CreateTokens { receiver, amount } => {
                            // save add is checked before
                            // (@TODO add_block / execute block would need to be
                            // somehow locked for multi threading)
                            let entry = self.accounts.get_mut(receiver).unwrap_or_else(|| {
                                // That should NEVER happen if rest of the code is untouched.
                                // Must be a multi-threading issue! Lock this function!
                                panic!(
                                    "You're trying to modify the balance of user {} who is not \
                                    available (anymore!) (Code: 93879823)",
                                    receiver
                                )
                            });

                            entry.balance += *amount;
                        },
                        BlockAction::ChangeStoreValue {key, value}  => {
                            let account = self.accounts.get_mut(&transaction.from).unwrap_or_else(|| {
                                panic!("Account {} does not exist anymore! (Code: 234723894)", &transaction.from)
                            });

                            if account.store.contains_key(key) {
                                account.store.remove_entry(key);
                                account.store.insert(key.clone(), value.clone());
                            } else {
                                account.store.insert(key.clone(), value.clone());
                            }
                        }, BlockAction::RemoveAccount(key) => {
                            self.accounts.remove_entry(key);
                        },
                        BlockAction::TransferTokens {to, amount} => {
                            let balance_from = self.accounts[&transaction.from].balance;
                            let balance_to = self.accounts[to].balance;

                            if balance_from >= *amount {
                                let new_balance_from = balance_from.checked_sub(*amount);
                                let new_balance_to = balance_to.checked_add(*amount);

                                if new_balance_to.is_some() && new_balance_from.is_some() {
                                    //let mut accounts = self.accounts.borrow_mut();
                                    //let mut to_account = accounts[to].borrow_mut();
                                    //let mut from_account = accounts[&transaction.from].borrow_mut();

                                    let mut acc = self.accounts.get_mut(to).unwrap();
                                    acc.balance = new_balance_to.unwrap();
                                    let mut acc = self.accounts.get_mut(&transaction.from).unwrap();
                                    acc.balance = new_balance_from.unwrap();
                                    //accounts[&transaction.from].balance = new_balance_from.unwrap();
                                    //accounts[to].balance = new_balance_to.unwrap();
                                } else {
                                    panic!("Under or overflow when transferring tokens! \
                                            (Code: 2093482093")
                                }
                            } else {
                                panic!("Balance not sufficient (Code: 209482309)")
                            }

                        }
                        _ => { // Rather save than sorry
                            panic!("Unknown command in Transaction #{} (Code: 24820348)", t_num)
                        }
                    }
                }
                t_num += 1;
            }
        }

        // remove transactions which did not succeed!
        if not_working_transactions.len() > 0 {
            for not_working_transaction in &not_working_transactions {
                // Find that not working transaction
                let block_idx: Vec<usize> = block.transactions.iter().enumerate().filter_map(move |(i, transaction)| {
                    if not_working_transaction == transaction {
                        Some(i)
                    } else {
                        None
                    }
                }).collect();

                if let Some(i) = block_idx.first() {
                    block.transactions.remove(*i);
                }
            }

            // Recalculate Block hash
            block.hash = Some(block.calculate_hash_str());
        }

        not_working_transactions
    }

    /// Will evaluate if the execution could be executed
    ///
    /// ** Warning
    /// Be aware that this check is local and only valid for ONE transaction at the TIME OF QUERY
    /// if for example multiple transactions transmit tokens, where each one separate is ok
    /// both executed together might not be!
    pub fn could_execute_transaction(&self, transaction: &Transaction) -> bool {
        let mut can_execute = true;
        // check signed
        let is_signed = transaction.is_signed();

        if transaction.is_signed() {
            can_execute &= transaction.check_signature();
        }

        match &transaction.record {
            BlockAction::CreateUserAccount(id) => {
                // Check if user doesnt exist already
                can_execute &= !self.accounts.contains_key(id.as_str());
            },
            BlockAction::CreateTokens {receiver, amount} => {
                can_execute &= self.accounts.contains_key(receiver);
                let new_amount = self.accounts[receiver].balance.checked_add(*amount);
                can_execute &= new_amount.is_some();
            },
            BlockAction::ChangeStoreValue {key, value}  => {
                can_execute &= self.accounts.contains_key(transaction.from.as_str());
            },
            BlockAction::RemoveAccount(acc) => {
                can_execute &= self.accounts.contains_key(acc);
            },
            BlockAction::TransferTokens {to, amount} => {
                can_execute &= self.accounts.contains_key(transaction.from.as_str());
                can_execute &= self.accounts.contains_key(to);
                let self_tokens = self.accounts[&transaction.from].balance;
                can_execute &= self_tokens >= *amount;

            }
            _ => { // Rather save than sorry
                can_execute = false;
            }
        }

        can_execute
    }

    pub fn n_blocks(&self) -> usize {
        self.blocks.len()
    }

    pub fn n_accounts(&self) -> usize {
        self.accounts.len()
    }

    pub fn n_user_accounts(&self) -> usize {
        self.accounts.iter().filter(
            |(_id, account)| {
                if let AccountType::User = account.acc_type {true} else {false}
            }).count()
    }

    pub fn get_block(&self, index: usize) -> Option<&Block> {
        if index < self.blocks.len() {
            let block = &self.blocks[index];
            Some(block)
        } else {
            None
        }

    }
}

/// A single operation to be stored on the chain
#[derive(Clone, Debug, PartialEq)]
pub enum BlockAction {
    CreateUserAccount(String),
    ChangeStoreValue {key: String, value: AccountValue},
    RemoveAccount(String),
    TransferTokens {to: String, amount: u128},
    CreateTokens {receiver: String, amount: u128}
}

/// We distinguish normal accounts from accounts with code
#[derive(Clone, Debug)]
pub enum AccountType {
    User,
    Contract
}

/// Single information type to be stored into an account.
/// This includes basically everything that can be turned into a String
/// We want it to be string-compatible to persist it easily somewhere
#[derive(Clone, Debug, PartialEq)]
pub struct AccountValue {
    pub value: String
}

impl AccountValue {
    pub fn as_bytes(&self) -> &[u8] {
        self.value.as_bytes()
    }
}

/// Actual Account
#[derive(Clone, Debug)]
pub struct Account {
    key: String,
    store: HashMap<String, AccountValue>,
    acc_type: AccountType,
    balance: u128,
}

impl Account {
    pub fn new(acc_type: AccountType, key: String, balance: u128) -> Self {
        return Self {
            acc_type,
            key,
            balance,
            store: HashMap::new()
        }
    }
}
