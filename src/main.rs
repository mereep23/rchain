#[macro_use] extern crate env_logger;
#[macro_use] extern crate log;
extern crate base64;
extern crate openssl;
extern crate blake2;

mod blockchain;
use blockchain::{Account, AccountType, BlockChain, Block};
use crate::blockchain::{Transaction, BlockAction, AccountValue};
use openssl::sign::{Signer, Verifier};
use openssl::rsa::Rsa;
use openssl::pkey::PKey;
use openssl::hash::MessageDigest;

fn main() {

    // Create key pairs
    // Generate a keypair
    println!("Creating 2 Key Pairs");
    let keypair1 = Rsa::generate(2048).unwrap();
    let keypair1 = PKey::from_rsa(keypair1).unwrap();
    let pub_key1: Vec<u8> = keypair1.public_key_to_der().unwrap();
    let priv_key1: Vec<u8> = keypair1.private_key_to_der().unwrap();
    let pub_key1_str: String = base64::encode_config(pub_key1.clone(), base64::URL_SAFE_NO_PAD); //pub_key1.iter().map(|c| *c as char).collect();

    let keypair2 = Rsa::generate(2048).unwrap();
    let keypair2 = PKey::from_rsa(keypair2).unwrap();
    let pub_key2: Vec<u8> = keypair2.public_key_to_der().unwrap();
    let priv_key2: Vec<u8> = keypair2.private_key_to_der().unwrap();
    let pub_key2_str: String = base64::encode_config(pub_key2.clone(), base64::URL_SAFE_NO_PAD);

    //println!("{}:{}, {}:{}", pub_key1.len(), pub_key1_str.len(), pub_key2.len(), pub_key2_str.len());

    // Create two dummy accounts with each one pub key
    println!("Creating genesis block with two accounts");
    let acc1 = Account::new(
        AccountType::User,
        pub_key1_str.clone().into(), 100);

    let acc2 = Account::new(
        AccountType::User,
        pub_key2_str.clone().into(), 101);

    let users = vec!{acc1, acc2};

    // Create Genesis (First) Block in the blockchain
    // The BC will accept anything that inside there (we could push ANY executable transaction here)
    let genesis = Block::new();

    // create the block chain with the empty block. User Create transactions are created on their own
    let mut bc = BlockChain::new(genesis,&users);

    println!("Blockchain has now {} blocks!", bc.n_blocks());
    // Check if block chain is valid
    println!("Blockchain is valid: {}\n", bc.is_valid());

    // Create some commands that will inhabit the users' storages and transfer tokens
    println!("Create 3 Actions and add them to the pool (One is signed)");
    let c1: BlockAction = BlockAction::ChangeStoreValue {key: "TestKey".into(), value: AccountValue {value: "TestValue".into()}};
    let mut t1: Transaction = Transaction::new(pub_key2_str.clone().into(),  c1, 123123);
    t1.sign_transaction(&priv_key2.clone());

    let c2: BlockAction = BlockAction::ChangeStoreValue {key: "TestKey".into(), value: AccountValue {value: "TestVal2".into()}};
    let mut t2: Transaction = Transaction::new(pub_key2_str.clone().into(),  c2, 2132134234);
    t2.sign_transaction(&priv_key2.clone());

    let c3: BlockAction = BlockAction::TransferTokens {to: pub_key2_str.clone().into(), amount: 100};
    let mut t3: Transaction = Transaction::new(pub_key1_str.clone().into(),  c3, 234234234);
    t3.sign_transaction(&priv_key1.clone());


    //println!("{:?}\n\n", &pub_key1.clone());
    println!("Transaction Correctly Signed? {}\n", t3.check_signature());

    // ... add them to pending transactions
    bc.add_transaction_request(t1);
    bc.add_transaction_request(t2);
    bc.add_transaction_request(t3);

    println!("Generate a dummy block having those 3 transaction inside\n");
    // ... generate a empty block with our two pending transaction (The chain generates the dummy for us!)
    let mut block = bc.generate_block_with_transactions().unwrap();

    /*// ... print out the block hashes for fun
    for i in 0..bc.n_blocks() {
        println!("Block {}: hash: {}, ", i, bc.get_block(i).unwrap().calculate_hash_str())
    }*/


    /*// print the whole chain
    println!("{:?}", bc);*/

    // print users stores only
    println!("User 1 before second block: {:?}", bc.get_user_data(pub_key2_str.clone().into()).unwrap());
    println!("User 2 before second block: {:?}\n\n", bc.get_user_data(pub_key1_str.clone().into()).unwrap());

    // Mine a new block (Hash must begin with RI in ASCII)
    // Try to find a hash
    println!("Trying to mine a valid block (Proof of Work)... this may take a while");
    for i in 0..256_000_000 /* Statistically this should emit one number*/{
        block.nonce = i; // Thats what we can change
        let hash = block.calculate_hash_str();
        let hash_byte_1= hash.as_bytes()[0];
        let hash_byte_2= hash.as_bytes()[1];
        let hash_byte_3= hash.as_bytes()[2];

        if hash_byte_1 == 82 && hash_byte_2 == 73  /* && hash_byte_3 == 67*/  {
            println!("Found valid hash starting with RI ({}) at nonce {}", i, hash);
            break;
        }
    }

    // Add block to block chain (if hash was wrong, it will not accept it
    println!("Blockchain add block {:?}\n", bc.add_block(block));

    println!("Blockchain has now {} blocks!", bc.n_blocks());
    // Check if block chain is valid
    println!("Blockchain is valid: {}", bc.is_valid());

    // ... and check if the user data is really persisted
    println!("First user {:?}", bc.get_user_data(pub_key1_str.clone().into()));
    println!("Second user {:?}", bc.get_user_data(pub_key2_str.clone().into()));
}
